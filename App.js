import React from 'react';
import Amplify, { Auth } from 'aws-amplify';
import AWSAppSyncClient, {
  AUTH_TYPE
} from "aws-appsync";
import { ApolloProvider } from 'react-apollo';
import Rehydrated from './components/rehydrated';
import Application from "./components/application";

const amplifyConfig = require("./amplify-config.json");
const appsyncConfig = require("./appsync-config.json");

// Amplify.Logger.LOG_LEVEL = "DEBUG";
Amplify.configure(amplifyConfig);

const client = new AWSAppSyncClient({
  ...appsyncConfig,
  auth: {
    type: AUTH_TYPE.AWS_IAM,
    credentials: () => Auth.currentCredentials()
  }
});

function AppWithApolloProvider() {
  return (
    <ApolloProvider client={client}>
      <Rehydrated>
        <Application />
      </Rehydrated>
    </ApolloProvider>
  );
}

export default AppWithApolloProvider;