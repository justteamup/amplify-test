# Amplify Test

## Initialize Expo project

```bash
expo init amplifyTest
```

Select option "blank" in the "Managed workflow".

Expo SDK version 37.0.10 is installed (which uses React Native 0.61 internally).

## Install and configure Amplify

```bash
expo install aws-amplify
```

Version 3.0.10 is installed.

Error because Expo needs NetInfo:

```bash
Unable to resolve "@react-native-community/netinfo" from "node_modules/@aws-amplify/datastore/lib-esm/sync/datastoreReachability/index.native.js"
Failed building JavaScript bundle.
```

Install NetInfo:

```bash
expo install @react-native-community/netinfo
```

Version 5.5.1 is installed.

## Install and configure AppSync and Apollo

```bash
expo install aws-appsync aws-appsync-react react-apollo
```

AWS AppSync version 3.0.0 and Apollo version 3.1.5 are installed.

Error:

```bash
Warning: Failed context type: The context `client` is marked as required in `Rehydrated`, but its value is `undefined`.
```

## Fix Rehydrated component error

The solution is to create a custom Rehydrated component:

https://github.com/awslabs/aws-mobile-appsync-sdk-js/issues/448#issuecomment-519447829

Now it works but with multiple warnings:

```bash
[Unhandled promise rejection: TypeError: _netinfo.default.getConnectionInfo is not a function. (In '_netinfo.default.getConnectionInfo()', '_netinfo.default.getConnectionInfo' is undefined)]
- node_modules/@redux-offline/redux-offline/lib/defaults/detectNetwork.native.js:96:14 in _callee2$
- node_modules/regenerator-runtime/runtime.js:45:44 in tryCatch
- node_modules/regenerator-runtime/runtime.js:274:30 in invoke
- node_modules/@babel/runtime/helpers/asyncToGenerator.js:2:6 in asyncGeneratorStep
- node_modules/@babel/runtime/helpers/asyncToGenerator.js:24:6 in _next
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:432021:14 in <unknown>
- node_modules/promise/setimmediate/core.js:45:7 in tryCallTwo
- node_modules/promise/setimmediate/core.js:200:23 in doResolve
- node_modules/promise/setimmediate/core.js:66:12 in Promise
- node_modules/@babel/runtime/helpers/asyncToGenerator.js:20:15 in <anonymous>
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:433325:17 in DetectNetwork
- node_modules/@redux-offline/redux-offline/lib/defaults/detectNetwork.native.js:224:24 in _default
- node_modules/@redux-offline/redux-offline/lib/index.js:70:17 in <anonymous>
- node_modules/redux/lib/applyMiddleware.js:37:30 in <anonymous>
- node_modules/aws-appsync/lib/store.js:82:4 in newStore
- node_modules/aws-appsync/lib/client.js:189:120 in AWSAppSyncClient
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:124652:39 in <unknown>
- node_modules/metro/src/lib/polyfills/require.js:322:6 in loadModuleImplementation
- node_modules/expo/AppEntry.js:3:0 in <global>
- node_modules/metro/src/lib/polyfills/require.js:322:6 in loadModuleImplementation
- node_modules/metro/src/lib/polyfills/require.js:201:45 in guardedLoadModule
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:435921:4 in global code
```

```bash
[Unhandled promise rejection: TypeError: handler is not a function. (In 'handler(nextState)', 'handler' is "connectionChange")]
- node_modules/@react-native-community/netinfo/src/internal/state.ts:65:9 in State#_handleInternetReachabilityUpdate
* [native code]:null in forEach
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:300316:35 in <unknown>
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:300479:22 in <unknown>
- node_modules/@react-native-community/netinfo/src/internal/internetReachability.ts:119:36 in Promise.race.then.then._catch$argument_0
- node_modules/promise/setimmediate/core.js:37:14 in tryCallOne
- node_modules/promise/setimmediate/core.js:123:25 in setImmediate$argument_0
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:146:14 in _callTimer
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:194:17 in _callImmediatesPass
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:458:30 in callImmediates
* [native code]:null in callImmediates
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:407:6 in __callImmediates
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:143:6 in __guard$argument_0
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:384:10 in __guard
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:142:17 in __guard$argument_0
* [native code]:null in flushedQueue
* [native code]:null in callFunctionReturnFlushedQueue
```

## Fix NetInfo version

If we install NetInfo version 4.7.0 we get an Expo error because Expo SDK 37 needs NetInfo version 5.5.1.

So we need to patch the aws-appsync module.

```bash
npm install --save-dev patch-package
```

Modify the redux-offline module to use the NetInfo version 5.5.1 (check the patches folder) and apply the patch:

```bash
npx patch-package @redux-offline/redux-offline
```

Now we can run the app on the iOS simulator without warnings.

## Run a query

Install GraphQL tag:

```bash
expo install graphql-tag
```

After run the app we get an error because aws-appsync needs react-apollo 2.x, then we need to uninstall the v3 and install the v2.

```bash
npm uninstall --save react-apollo
npm install --save react-apollo@2
```

React Apollo version 2.5.8 is installed, but now we have another error:

```bash
error Unable to resolve "apollo-client" from "node_modules/aws-appsync-subscription-link/lib/subscription-handshake-link.js"
Failed building JavaScript bundle.
```

This is because aws-appsync-subscription-link needs apollo-client 2.x. Install:

```bash
npm install --save apollo-client@2
```

And another error:

```bash
Error:
(0, _reactApollo.getApolloContext) is not a function. (In '(0, _reactApollo.getApolloContext)()', '(0, _reactApollo.getApolloContext)' is undefined)
```

We think this error is from the custom Rehydrated component we are using for the previous Apollo client v3 (now we are using v2) so we remove it and re-install AWS AppSync React to use the original Rehydrated component:

```bash
expo install aws-appsync-react
```

We have more warnings from NetInfo because we need to re-patch redux-offline, so we edit the file and apply the patch:

```bash
npx patch-package @redux-offline/redux-offline
```

Now we get a new warning from AWS AppSync React:

```bash
[Unhandled promise rejection: TypeError: undefined is not an object (evaluating 'netinfo_1.default.isConnected.fetch')]
- node_modules/aws-appsync-react/lib/rehydrated-rn.js:83:75 in __generator$argument_1
- node_modules/aws-appsync-react/lib/rehydrated-rn.js:46:27 in step
- node_modules/aws-appsync-react/lib/rehydrated-rn.js:18:62 in fulfilled
- node_modules/promise/setimmediate/core.js:36:6 in tryCallOne
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:438003:27 in <unknown>
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:142:10 in _callTimer
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:193:17 in _callImmediatesPass
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:410827:33 in callImmediates
* [native code]:null in callImmediates
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:406:13 in __callImmediates
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:142:23 in __guard$argument_0
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:383:10 in __guard
* http://127.0.0.1:19001/node_modules/expo/AppEntry.bundle?platform=ios&dev=true&minify=false&hot=false:438630:21 in flushedQueue
* [native code]:null in flushedQueue
* [native code]:null in invokeCallbackAndReturnFlushedQueue
```

## Create a custom rehydrate component

We uninstall aws-appsyn-react

```bash
npm uninstall --save aws-appsync-react
```

Re-create a custom Rehydrated component for Apollo client v2.4 and the query is executed successfully:

```bash
loading, error, data: true undefined Object {}
loading, error, data: false undefined Object {
  "debug": Object {
    "__typename": "DebugStatus",
    "status": "OK",
  },
}
```

But we get this warning:

```bash
Warning: componentWillReceiveProps has been renamed, and is not recommended for use. See https://fb.me/react-async-component-lifecycle-hooks for details.

* Move data fetching code or side effects to componentDidUpdate.
* If you're updating state whenever props change, refactor your code to use memoization techniques or move it to static getDerivedStateFromProps. Learn more at: https://fb.me/react-derived-state
* Rename componentWillReceiveProps to UNSAFE_componentWillReceiveProps to suppress this warning in non-strict mode. In React 17.x, only the UNSAFE_ name will work. To rename all deprecated lifecycles to their new names, you can run `npx react-codemod rename-unsafe-lifecycles` in your project source folder.

Please update the following components: Query
- node_modules/expo/build/environment/muteWarnings.fx.js:18:23 in warn
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:5864:19 in printWarning
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:5892:25 in lowPriorityWarning
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:6135:8 in ReactStrictModeWarnings.flushPendingUnsafeLifecycleWarnings
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:20377:6 in flushRenderPhaseStrictModeWarningsInDEV
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:19606:41 in commitRootImpl
* [native code]:null in commitRootImpl
- node_modules/scheduler/cjs/scheduler.development.js:643:23 in unstable_runWithPriority
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:19590:4 in commitRoot
* [native code]:null in commitRoot
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:18709:28 in runRootCallback
* [native code]:null in runRootCallback
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:5642:32 in runWithPriority$argument_1
- node_modules/scheduler/cjs/scheduler.development.js:643:23 in unstable_runWithPriority
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:5638:22 in flushSyncCallbackQueueImpl
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:5627:28 in flushSyncCallbackQueue
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:18556:30 in scheduleUpdateOnFiber
- node_modules/react-native/Libraries/Renderer/implementations/ReactNativeRenderer-dev.js:11484:17 in dispatchAction
* [native code]:null in dispatchAction
* components/rehydrated.js:10:7 in <anonymous>
- node_modules/regenerator-runtime/runtime.js:45:44 in tryCatch
- node_modules/regenerator-runtime/runtime.js:274:30 in invoke
- node_modules/regenerator-runtime/runtime.js:45:44 in tryCatch
- node_modules/regenerator-runtime/runtime.js:135:28 in invoke
- node_modules/regenerator-runtime/runtime.js:145:19 in PromiseImpl.resolve.then$argument_0
- node_modules/promise/setimmediate/core.js:37:14 in tryCallOne
- node_modules/promise/setimmediate/core.js:123:25 in setImmediate$argument_0
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:146:14 in _callTimer
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:194:17 in _callImmediatesPass
- node_modules/react-native/Libraries/Core/Timers/JSTimers.js:458:30 in callImmediates
* [native code]:null in callImmediates
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:407:6 in __callImmediates
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:143:6 in __guard$argument_0
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:384:10 in __guard
- node_modules/react-native/Libraries/BatchedBridge/MessageQueue.js:142:17 in __guard$argument_0
* [native code]:null in flushedQueue
* [native code]:null in invokeCallbackAndReturnFlushedQueue
```
