import React, { useEffect, useState } from 'react';
import { withApollo } from 'react-apollo';
import AWSAppSyncClient from 'aws-appsync';
import { Text } from "react-native"

function Rehydrated({ client, children }) {
  const [rehydrated, setHydrated] = useState(false);

  async function hydrate() {
    await client.hydrated();
    setHydrated(true);
  }

  useEffect(() => {
    if (client instanceof AWSAppSyncClient) {
      hydrate();
    }
  }, [client]);

  return rehydrated ? children : null;
};

export default withApollo(Rehydrated);