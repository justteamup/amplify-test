import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Query } from "react-apollo";
import gql from "graphql-tag";

const DEBUG_QUERY = gql`
  query Debug($filter: DebugInput!) {
    debug(filter: $filter) {
      status
    }
  }
`;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function Application() {
  return (
    <Query
      query={DEBUG_QUERY}
      variables={{ filter: { data: "foo" } }}
    >
      {({ loading, error, data }) => {
        console.log("loading, error, data:", loading, error, data);

        return (
          <View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
          </View>
        );
      }}
    </Query>
  );
}

export default Application;